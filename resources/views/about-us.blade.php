@extends('layouts.app')

@section('title', 'Giới thiệu')

@section('content')
    <div class="about-us rounded">
        <div class="row justify-content-center about-us-inner">
            <div class="col-md-12">
                <h1>Giới thiệu</h1>
                <br>
                <p>Sản phẩm Website là kết quả của Luận văn Thạc sĩ "Đánh giá sự biến đổi của một số đặc trưng nhiệt độ và lượng mưa trên khu vực Việt Bắc và xây dựng bộ công cụ khai thác thông tin hiệu quả"</p>
                <p><b>Giáo viên hướng dẫn:</b> GS.TS Phan Văn Tân</p>
                <p><b>Học viên:</b> Bùi Nam Tuyển</p>
            </div>
        </div>
    </div>
@endsection
