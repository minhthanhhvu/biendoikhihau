@extends('layouts.app')

@section('title', 'Trang chủ')

@section('css')
    @include('shared.chart_css')
@endsection

@section('content')
    {!! Form::open(['route' => 'home', 'method' => 'GET', 'id' => 'form-home']) !!}
    @include('shared.chart_content')
    {!! Form::close() !!}
@endsection

@section('js')
    @include('shared.chart_js')
@endsection
