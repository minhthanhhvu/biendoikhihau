﻿@if($errors->any())
	<div id="alert-box" class="alert alert-danger">
		<b>Dữ liệu nhập vào không hợp lệ</b>
		<ul>
		@if($errors->any())
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		@endif
		</ul>
	</div>
@endif

@if(Session::has('flash_message'))
	<div class="col-xs-12">
		<div class="alert alert-info fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			{{ Session::get('flash_message') }}
		</div>
	</div>
@endif