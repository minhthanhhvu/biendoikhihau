<div class="row justify-content-center rounded">
    <div class="col-md-12">
        <div class="dvls_maps_wrap rounded">
            <div class="dvls_maps_row dvls_maps_header">
                <div class="dvls_maps_3col">
                    <div class="dvls_maps_col">
                        {!! Form::label('type', 'Loại số liệu') !!}
                        {!! Form::select('type', $types, $type, ['id' => 'type']) !!}
                    </div>
                    @if(isset($is_report_with_month))
                        <div class="dvls_maps_col" @if(!$is_report_with_month) style="display: none;" @endif>
                            {!! Form::label('report_month', 'Tháng báo cáo') !!}
                            {!! Form::select('report_month', $report_months, $report_month, ['id' => 'report_month']) !!}
                        </div>
                    @endif
                </div>
            </div>
            {{--<div class="dvls_maps_row dvls_maps_header">
                <div class="dvls_maps_header_left">Tìm trạm</div>
                <div class="dvls_maps_header_right">
                    <form action="" method="get">
                        <div class="dvls_maps_3col">
                            <div class="dvls_maps_col">
                                <select name="city" id="dvls_city" data-value="">
                                    <option value="null">Chọn tỉnh thành</option>
                                </select>
                            </div>
                            <div class="dvls_maps_col">
                                <select name="district" id="dvls_district" data-value="">
                                    <option value="null">Chọn quận huyện</option>
                                </select>
                            </div>
                            <div class="dvls_maps_col">
                                <input value="Tìm trạm" type="submit" class="dvls-submit"/>
                            </div>
                        </div>

                        <div class="dvls_maps_col">
                            <a href="javascript:void(0);" class="dvls_near_you" title="Tìm trạm ở gần bạn (bán kính <={{ $dvls_settings['radius'] }} km)">Tìm trạm ở gần bạn (bán kính <= {{ $dvls_settings['radius'] }} km)</a>
                        </div>
                    </form>
                </div>
            </div>--}}
            <div class="dvls_maps_container">
                <div class="dvls_maps_row dvls_maps_body">
                    <div class="dvls_maps_sidebar">
                        <div class="dvls_maps_sidebar_content">
                            <div class="dvls_result_status">DANH SÁCH TRẠM</div>
                            <div class="dvls_result_wrap">Đang tải...</div>
                        </div>
                    </div>
                    <div class="dvls_maps_main">
                        <div id="dvls_maps" data-lat="{{ $dvls_settings['lat_default'] }}" data-lng="{{ $dvls_settings['lng_default'] }}"></div>
                        <div class="dvls_maps_note">
                            <style>
                                .not-ece{
                                    float: left;
                                    width: 170px;
                                }
                                .not-ece-left{
                                    float: left;
                                    width: 50px;
                                    height: 200px;
                                }
                                .not-ece-right{
                                    float: left;
                                    width: 100px;
                                    height: 200px;
                                    position: relative;
                                    margin-left: 20px;
                                }
                                .not-ece-right-top{
                                    position: absolute;
                                    top: 0;
                                    left: 0;
                                }
                                .not-ece-right-middle-1{
                                    position: absolute;
                                    top: 20%;
                                    left: 0;
                                }
                                .not-ece-right-middle-2{
                                    position: absolute;
                                    top: 42%;
                                    left: 0;
                                }
                                .not-ece-right-middle-3{
                                    position: absolute;
                                    top: 65%;
                                    left: 0;
                                }
                                .not-ece-right-bottom{
                                    position: absolute;
                                    bottom: 0;
                                    left: 0;
                                }
                            </style>

                            <h2>* Chú thích</h2>
                            @if($type_obj['is_trendline'])
                                <div class="row">
                                    <div class="col-md-4">
                                        <p><b>1. Xu hướng tăng ({{ $type_obj['don_vi'] }})</b></p>
                                        <div class="not-ece">
                                            <div class="not-ece-left" style="background-image: linear-gradient(#{{ $type_obj['increase_color_to'] }}, #{{ $type_obj['increase_color_from'] }});"></div>
                                            <div class="not-ece-right">
                                                <span class="not-ece-right-top">{{ round($type_obj['increase_max'], 5) }}</span>
                                                <span class="not-ece-right-middle-1">{{ round($type_obj['increase_min'] + (($type_obj['increase_max'] - $type_obj['increase_min']) / 100) * 75, 5) }}</span>
                                                <span class="not-ece-right-middle-2">{{ round($type_obj['increase_min'] + (($type_obj['increase_max'] - $type_obj['increase_min']) / 100) * 50, 5) }}</span>
                                                <span class="not-ece-right-middle-3">{{ round($type_obj['increase_min'] + (($type_obj['increase_max'] - $type_obj['increase_min']) / 100) * 25, 5) }}</span>
                                                <span class="not-ece-right-bottom">{{ round($type_obj['increase_min'], 5) }}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <p><b>2. Xu hướng giảm ({{ $type_obj['don_vi'] }})</b></p>
                                    <div class="not-ece">
                                        <div class="not-ece-left" style="background-image: linear-gradient(#{{ $type_obj['decrease_color_to'] }}, #{{ $type_obj['decrease_color_from'] }});"></div>
                                        <div class="not-ece-right">
                                            <span class="not-ece-right-top">{{ round($type_obj['decrease_max'], 5) }}</span>
                                            <span class="not-ece-right-middle-1">{{ round($type_obj['decrease_min'] + (($type_obj['decrease_max'] - $type_obj['decrease_min']) / 100) * 75, 5) }}</span>
                                            <span class="not-ece-right-middle-2">{{ round($type_obj['decrease_min'] + (($type_obj['decrease_max'] - $type_obj['decrease_min']) / 100) * 50, 5) }}</span>
                                            <span class="not-ece-right-middle-3">{{ round($type_obj['decrease_min'] + (($type_obj['decrease_max'] - $type_obj['decrease_min']) / 100) * 25, 5) }}</span>
                                            <span class="not-ece-right-bottom">{{ round($type_obj['decrease_min'], 5) }}</span>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            @else
                                <p><b>{{ $type_obj['mo_ta'] }} ({{ $type_obj['don_vi'] }})</b></p>
                                <div class="not-ece">
                                    <div class="not-ece-left" style="background-image: linear-gradient(#{{ $type_obj['color_to'] }}, #{{ $type_obj['color_from'] }});"></div>
                                    <div class="not-ece-right">
                                        <span class="not-ece-right-top">{{ round($type_obj['max'], 1) }}</span>
                                        <span class="not-ece-right-middle-1">{{ round($type_obj['min'] + (($type_obj['max'] - $type_obj['min']) / 100) * 75, 1) }}</span>
                                        <span class="not-ece-right-middle-2">{{ round($type_obj['min'] + (($type_obj['max'] - $type_obj['min']) / 100) * 50, 1) }}</span>
                                        <span class="not-ece-right-middle-3">{{ round($type_obj['min'] + (($type_obj['max'] - $type_obj['min']) / 100) * 25, 1) }}</span>
                                        <span class="not-ece-right-bottom">{{ round($type_obj['min'], 1) }}</span>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@foreach($stations as $station)
    <div class="modal fade" id="modal{{ $station['Ma'] }}" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Trạm {{ $station['Ten'] }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <canvas id="chart{{ $station['Ma'] }}" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endforeach
