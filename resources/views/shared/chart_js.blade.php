<script type="text/javascript">
    $(document).ready(function(){
        var type_obj = @json($type_obj);

        @foreach($stations as $station)
            var chartDatasets{{ $station['Ma'] }} = [
                {
                    label               : '{{ $type_obj['ten_loai_so_lieu'] }} @if($type_obj['is_trendline'])                                     Đường xu thế: {{ $station['equation'] }} @endif',
                    borderColor         : type_obj.borderColor,
                    backgroundColor     : type_obj.backgroundColor,
                    data                : @json($station['report_data']),
                },
            ];

            if(type_obj.is_trendline){
                chartDatasets{{ $station['Ma'] }}[0].trendlineLinear = {
                    style: "rgba(237,30,6,0.9)",
                        lineStyle: "solid",
                        width: 2
                };
            }

            var chartData{{ $station['Ma'] }} = {
                labels  : @json($station['report_time']),
                datasets: chartDatasets{{ $station['Ma'] }}
            };

            var chartCanvas{{ $station['Ma'] }} = $('#chart{{ $station['Ma'] }}').get(0).getContext('2d');
            var chartOptions{{ $station['Ma'] }} = {
                maintainAspectRatio : false,
                responsive : true,
                scales: {
                    xAxes: [{
                        ticks: {
                            userCallback: function(item) {
                                return type_obj.time_unit +' '+ item;
                            },
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            @if($type_obj['don_vi'] == 'ºC')
                                suggestedMin: 10,
                                suggestedMax: 35,
                            @else
                                beginAtZero: true,
                            @endif
                            userCallback: function(item) {
                                return item +' '+ type_obj.don_vi;
                            },
                        }
                    }]
                },
                tooltips: {
                    enabled: true,
                    mode: 'single',
                    callbacks: {
                        title: function(tooltipItems, data) {
                            //Return value for title
                            return type_obj.time_unit +' '+ tooltipItems[0].xLabel;
                        },
                        label: function(tooltipItems, data) {
                            return tooltipItems.yLabel +' '+ type_obj.don_vi;
                        },
                    }
                },
            };
            var chart{{ $station['Ma'] }} = new Chart(chartCanvas{{ $station['Ma'] }}, {
                type: type_obj.chart_type,
                data: chartData{{ $station['Ma'] }},
                options: chartOptions{{ $station['Ma'] }},
            });
        @endforeach

        $('#type').on('change', function(){
            $('#form-home').submit();
        });
        $('#report_month').on('change', function(){
            $('#form-home').submit();
        });

        var station_data =  @json($stations);
        var stations = {
            'success'   : true,
            'data'      : station_data,
        };

        var local_address =  @json($cities);
        var devvn_localstore_array = {
            'ajaxurl'           : '/',
            'siteurl'           : '/',
            'local_address'     :  local_address,
            'maps_zoom'     :  '{{ $dvls_settings['maps_zoom'] }}',
            'select_text'   :  'Chọn quận huyện',
            'lat_default'   :  '{{ $dvls_settings['lat_default'] }}',
            'lng_default'   :  '{{ $dvls_settings['lng_default'] }}',
            'close_icon'    :  'images/close-btn.png',
            'marker_default':  $('<i />').html('{{ $dvls_settings['marker_icon'] }}').text(),
            'labels'        : {
                'disallow_labels'   :  '{{ $dvls_settings['disallow_labels'] }}',
                'get_directions'    :  '{{ $dvls_settings['get_directions'] }}',
                'text_open'         :  '{{ $dvls_settings['text_open'] }}',
                'text_phone'        :  '{{ $dvls_settings['text_phone'] }}',
                'text_hotline'      :  '{{ $dvls_settings['text_hotline'] }}',
                'text_email'        :  '{{ $dvls_settings['text_email'] }}',
            }
        };

        var dvls_city = devvn_localstore_array.local_address;
        var citySelect = $('#dvls_city');
        var districtSelect = $('#dvls_district');
        var oldValueCity = citySelect.data('value');
        var oldValueDistrict = districtSelect.data('value');

        $(dvls_city).each(function(index, value){
            var thisChecked = '';
            if(oldValueCity == value.id) {
                thisChecked = 'selected="selected"';
                $(value.district).each(function(index, value) {
                    var thisChecked = '';
                    if(oldValueDistrict == value.id) {
                        thisChecked = 'selected="selected"';
                    }
                    districtSelect.append('<option value="' + value.id + '" '+thisChecked+'>' + value.name + '</option>');
                });
            }
            citySelect.append('<option value="'+value.id+'" '+thisChecked+'>'+value.name+'</option>');
        });
        $(citySelect).on('change',function(){
            var thisval = $(this).val();
            districtSelect.html('<option value="null" selected>'+devvn_localstore_array.select_text+'</option>');
            $(dvls_city).each(function(index, value){
                if(thisval == value.id){
                    $(value.district).each(function(index, value) {
                        districtSelect.append('<option value="' + value.id + '">' + value.name + '</option>');
                    });
                    return false;
                }
            });
        });

        var autocomplete,map,markers = [],infoWindow,geocoder,dvls_loading = false;
        var mapDiv = $('#dvls_maps');

        function dvls_initMap() {
            var dvls_center = {lat: mapDiv.data('lat'), lng: mapDiv.data('lng')};
            map = new google.maps.Map(document.getElementById('dvls_maps'), {
                zoom: parseInt(devvn_localstore_array.maps_zoom),
                center: dvls_center,
            });
            infoWindow = new google.maps.InfoWindow();

            dvls_lastest_store();
        }
        dvls_initMap();
        $('.dvls_near_you').on('click',function () {
            var thisDisallow = $(this).data('disallow');
            if(thisDisallow == 1) return false;
            devvn_findstore_nearyou();
            return false;
        });
        function dvls_notsupport_geocoder(){
            $('.dvls_near_you').remove();
        }
        function dvls_disallow_geocoder(){
            $('.dvls_near_you').attr('data-disallow','1').html(devvn_localstore_array.labels.disallow_labels);
        }
        function devvn_findstore_nearyou(){
            if (navigator.geolocation) {
                dvls_before_load();
                navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
            } else {
                dvls_notsupport_geocoder();
                dvls_lastest_store();
            }
            geocoder = new google.maps.Geocoder();
        }
        function errorFunction() {
            dvls_disallow_geocoder();
            dvls_lastest_store();
        }
        function successFunction(position) {
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;
            var data = [];
            data['lat'] = lat;
            data['lng'] = lng;
            data['near'] = true;
            dvls_firstload_store(data);
        }

        function dvls_lastest_store() {
            var data = [];
            data['near'] = false;
            dvls_firstload_store(data);
        }

        function dvls_before_load(){
            dvls_loading = true;
            $('.dvls_maps_body').addClass('devvn_loading');
        }


        function dvls_ajax_load_success(response){
            if(response.success) {
                var maps_data = response.data;
                var bounds = new google.maps.LatLngBounds();
                var dataMarker = [];
                clearLocations(maps_data.length);
                $('.dvls_result_wrap').html('');
                for (var i = 0; i < maps_data.length; i++)
                {
                    //console.log(maps_data[i].Ten+': '+maps_data[i].m);
                    var lat = (maps_data[i].ViDo) ? maps_data[i].ViDo : '';
                    var lng = (maps_data[i].KinhDo) ? maps_data[i].KinhDo : '';
                    var latlng = new google.maps.LatLng(lat,lng);
                    dataMarker['stt'] = i;
                    dataMarker['id'] = (maps_data[i].Ma) ? maps_data[i].Ma : '';
                    dataMarker['name'] = (maps_data[i].Ten) ? maps_data[i].Ten : '';
                    dataMarker['city'] = (maps_data[i].city.Ten) ? maps_data[i].city.Ten : '';
                    dataMarker['district'] = (maps_data[i].district.TenQuanHuyen) ? maps_data[i].district.TenQuanHuyen : '';
                    if(type_obj.is_trendline){
                        dataMarker['m'] = maps_data[i].m;
                        dataMarker['equation'] = maps_data[i].equation;
                    }else{
                        dataMarker['avg'] = maps_data[i].avg;
                    }
                    dataMarker['marker'] = maps_data[i].marker_icon;
                    dataMarker['h_marker'] = 35;
                    dataMarker['maps_lat'] = lat;
                    dataMarker['maps_lng'] = lng;
                    dataMarker['latlng'] = latlng;
                    dataMarker['high'] = (maps_data[i].DoCao) ? maps_data[i].DoCao : '';

                    createMarker(dataMarker);
                    bounds.extend(latlng);
                    var has_thumb = '';
                    if(dataMarker['thumb']) has_thumb = 'has_thumb';

                    $html = '<div data-id="'+i+'" data-lat="'+lat+'" data-lng="'+lng+'" class="dvls_result_item '+has_thumb+'">';
                    if(dataMarker['thumb']) {
                        $html += '<div class="dvls_result_thumb"><img src="'+dataMarker['thumb']+'" alt=""></div>';
                    }
                    $html += '<div class="dvls_result_infor">';
                    $html += dataMarker['name'];
                    //$html += '<p><a href="/detail?station_id='+dataMarker.id+'">'+dataMarker['name']+'</p>';
                    $html += '</div>';
                    $html += '</div>';

                    $('.dvls_result_wrap').append($html);
                }
                if (maps_data.length){
                    var listener = google.maps.event.addListener(map, "idle", function() {
                        google.maps.event.removeListener(listener);
                    });
                    map.fitBounds(bounds);
                    if(maps_data.length == 1){
                        map.setZoom(parseInt(devvn_localstore_array.maps_zoom));
                    }
                    $(".dvls_result_wrap").on('click','.dvls_result_item',function() {
                        var markerNum = $(this).data('id');
                        var latsvl = $(this).data('lat');
                        var lngsvl = $(this).data('lng');
                        google.maps.event.trigger(markers[markerNum], 'click');
                        map.setZoom(parseInt(devvn_localstore_array.maps_zoom));
                        var b = new google.maps.LatLng(latsvl,lngsvl);
                        map.setCenter(b);
                        $(".dvls_result_wrap .dvls_result_item").removeClass('active');
                        $(this).addClass('active');
                    });
                    $('.dvls_result_status strong').html(maps_data.length);
                    $('.dvls_result_status').addClass('show');
                }
            }else {
                clearAllData();
            }
            $('.dvls_maps_body').removeClass('devvn_loading');
        }

        var lastInfobox;
        var lastClickedMarker;

        function createMarker(dataMarker){
            var i = dataMarker.stt;
            var id = dataMarker.id;
            var h_marker = (dataMarker.h_marker) ? dataMarker.h_marker : 35;

            var html = '';
            html += '<div class="item infobox" data-id="'+i+'" >';
            html += '<div class="item_infobox_infor">';
            html += '<h3>Trạm '+dataMarker.name+'</h3>';
            html += '<p>Vị trí: '+dataMarker.district+', '+dataMarker.city+'</p>';
            if(type_obj.is_trendline){
                html += '<p>Đường xu thế: '+ dataMarker.equation +'</p>';
            }else{
                html += '<p>'+ type_obj.ten_loai_so_lieu +': '+ dataMarker.avg.toFixed(1) +' '+ type_obj.don_vi +'</p>';
            }
            //html += '<a href="/detail?station_id='+dataMarker.id+'">'+devvn_localstore_array.labels.get_directions+'</a>';
            html += '</div>';
            html += '</div>';

            //Thêm icon map cho mỗi địa chỉ
            var marker = new MarkerWithLabel({
                position: dataMarker.latlng,
                draggable: true,
                raiseOnDrag: true,
                icon: ' ',
                map: map,
                labelContent: dataMarker.marker,
                labelAnchor: new google.maps.Point(22, 50),
                animation: google.maps.Animation.DROP,
                position: dataMarker.latlng
            });
            marker.setMap(map);

            var sWidth = -150;
            if (matchMedia('only screen and (max-width: 500px)').matches) {
                sWidth = -110;
            }

            google.maps.event.addListener(marker, 'click', function() {
                $('#modal'+id).modal('toggle');

                infoboxOptions = {
                    content: html,
                    disableAutoPan: false,
                    pixelOffset: new google.maps.Size(sWidth, -h_marker),
                    zIndex: null,
                    alignBottom: true,
                    boxClass: "infobox-wrapper",
                    closeBoxMargin: "15px 0px 0px 0px",
                    closeBoxURL: devvn_localstore_array.close_icon,
                    infoBoxClearance: new google.maps.Size(1, 1),
                    enableEventPropagation: false
                };

                if( lastInfobox != undefined ){
                    lastInfobox.close();
                }
                markers[i].infobox = new InfoBox(infoboxOptions);
                markers[i].infobox.open(map, this);
                lastInfobox = markers[i].infobox;
                $('.dvls_result_item.active').removeClass("active");
                $('.dvls_result_item[data-id='+i+']').addClass("active");
                google.maps.event.addListener(markers[i].infobox,'closeclick',function(){
                    $('.dvls_result_item.active').removeClass("active");
                });
            });

            markers.push(marker);
        }

        function formatDate(date) {
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0'+minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            return date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear() + "  " + strTime;
        }

        function dvls_firstload_store(data) {
            var near = (data['near']) ? true : false;
            var lat = (data['lat']) ? data['lat'] : '';
            var lng = (data['lng']) ? data['lng'] : '';
            var nonce = $('#dvls_nonce').val();
            var action = 'dvls_loadlastest_store';

            dvls_before_load();
            dvls_loading = false;
            dvls_ajax_load_success(stations);
        }

        function clearAllData(){
            $('.dvls_result_wrap').html('');
            clearLocations();
            $('.dvls_result_status strong').html('0');
            $('.dvls_result_status').addClass('show');
            var b = new google.maps.LatLng(devvn_localstore_array.lat_default,devvn_localstore_array.lng_default);
            map.setCenter(b);
        }
        /*function dvls_loadresult(){
            var nonce = $('#dvls_nonce').val();
            var cityid = $('#dvls_city').val();
            var districtid = $('#dvls_district').val();
            if(!dvls_loading) {
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: devvn_localstore_array.ajaxurl,
                    data: {
                        action: "dvls_load_localstores",
                        cityid: cityid,
                        districtid: districtid,
                        nonce: nonce
                    },
                    context: this,
                    beforeSend: function () {
                        dvls_before_load();
                    },
                    success: function (response) {
                        dvls_loading = false;
                        dvls_ajax_load_success(response);
                    },
                    error: function () {
                        $('.dvls_maps_body').removeClass('devvn_loading');
                        dvls_loading = false;
                    }
                });
            }
        }*/

        function clearLocations(n){
            infoWindow.close();
            for (var i = 0; i < markers.length; i++)
                markers[i].setMap(null);

            markers.length = 0;
        }
        /*$('.dvls-submit').on('click',function(){
            dvls_loadresult();
            return false;
        });*/
    });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ $dvls_settings['maps_api'] }}" defer></script>
<script type="text/javascript" src="{{ asset('js/markerwithlabel.js') }}" defer></script>
<script src="{{ asset('js/devvn-localstore-jquery.js') }}" defer></script>
