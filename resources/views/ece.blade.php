@extends('layouts.app')

@section('title', 'Xu thế biến đổi')

@section('css')
    @include('shared.chart_css')
@endsection

@section('content')
    {!! Form::open(['route' => 'ece', 'method' => 'GET', 'id' => 'form-home']) !!}
    @include('shared.chart_content')
    {!! Form::close() !!}
@endsection

@section('js')
    @include('shared.chart_js')
    <script src="{{ asset('admin-lte/plugins/chartjs-plugin-trendline/dist/chartjs-plugin-trendline.min.js') }}"></script>
@endsection
