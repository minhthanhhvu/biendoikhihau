<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});
Auth::routes();
/*
Route::get('/register', function () {
    return redirect('login');
});
*/
Route::get('/', 'HomeController@index')->name('home');
Route::get('/ece', 'EceController@index')->name('ece');
Route::get('/gioi-thieu', 'AboutUsController@index')->name('about-us');


