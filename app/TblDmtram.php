<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblDmtram extends Model
{
    protected $table = 'tbl_dmtram';
    protected $primaryKey = 'Ma';
    public $incrementing = false;

    public function city()
    {
    	return $this->belongsTo('App\TblDmhanhchinh', 'uuidHanhChinh');
    }

    public function district()
    {
    	return $this->belongsTo('App\TblDmquanhuyen', 'uuidQuanHuyen');
    }

    public function results()
    {
        return $this->hasMany('App\Dulieu', 'ma_tram');
    }
}
