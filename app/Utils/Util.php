<?php

namespace App\Utils;

use App\Dulieu;
use App\Loaisolieu;
use App\TblDmtram;
use App\TblDmhanhchinh;
use MathPHP\Statistics\Regression;
use DB;

class Util
{
    public function gradient($HexFrom, $HexTo, $ColorSteps) {
        $FromRGB['r'] = hexdec(substr($HexFrom, 0, 2));
        $FromRGB['g'] = hexdec(substr($HexFrom, 2, 2));
        $FromRGB['b'] = hexdec(substr($HexFrom, 4, 2));

        $ToRGB['r'] = hexdec(substr($HexTo, 0, 2));
        $ToRGB['g'] = hexdec(substr($HexTo, 2, 2));
        $ToRGB['b'] = hexdec(substr($HexTo, 4, 2));

        $StepRGB['r'] = ($FromRGB['r'] - $ToRGB['r']) / ($ColorSteps - 1);
        $StepRGB['g'] = ($FromRGB['g'] - $ToRGB['g']) / ($ColorSteps - 1);
        $StepRGB['b'] = ($FromRGB['b'] - $ToRGB['b']) / ($ColorSteps - 1);

        $gradientColors = array();

        for($i = 0; $i <= $ColorSteps; $i++) {
            $RGB['r'] = floor($FromRGB['r'] - ($StepRGB['r'] * $i));
            $RGB['g'] = floor($FromRGB['g'] - ($StepRGB['g'] * $i));
            $RGB['b'] = floor($FromRGB['b'] - ($StepRGB['b'] * $i));

            $HexRGB['r'] = sprintf('%02x', ($RGB['r']));
            $HexRGB['g'] = sprintf('%02x', ($RGB['g']));
            $HexRGB['b'] = sprintf('%02x', ($RGB['b']));

            $gradientColors[] = implode(NULL, $HexRGB);
        }
        $gradientColors = array_filter($gradientColors, function($val){
            return (strlen($val) == 6 ? true : false );
        });
        return $gradientColors;
    }

    public function getReport($request, $is_ece = false)
    {
        $type = $request->input('type');
        $report_month =  $request->input('report_month');

        $trendline_types =  ['Rnam', 'Ttbnam', 'Txnam', 'Tnnam'];
        $temp_types = ['Ttbnam', 'Txnam', 'Tnnam', 'Tnthang', 'Ttbthang', 'Txthang',
            'dtr', 'tnn', 'tnx', 'txn', 'txx'];
        $amount_of_rain_types = ['Rthang'];

        if(in_array($type, config('app.ece.month_and_year_type'))){
            $is_report_with_month = true;
        }else{
            $is_report_with_month = false;
        }

        $report_months = [];
        for($i=1; $i<=12; $i++){
            $report_months[$i] = 'Tháng '.$i;
        }
        if(!$report_month){
            $report_month = 1;
        }

        $types = Loaisolieu::query();
        if($is_ece){
            $types = $types->where('is_ece', 1);
        }else{
            $types = $types->where('is_ece', 0);
        }
        $types = $types->get()->pluck('ten_loai_so_lieu', 'loai_so_lieu')->toArray();

        if(!$type){
            $type = array_key_first($types);
        }
        $type_obj = Loaisolieu::where('loai_so_lieu', $type)->first();

        if(in_array($type, $trendline_types) || $is_ece){
            $type_obj->is_trendline = true;
        }else{
            $type_obj->is_trendline = false;
        }

        $dvls_settings = [
            'maps_api'      =>  config('app.google_maps_api_key'),
            'lat_default'   =>  '21.020799',
            'lng_default'   =>  '105.809476',
            'maps_zoom'     =>  17,
            'marker_icon'   =>  '<i class="fas fa-circle" style="color:rgba(255,255,255,1); font-size: 30px; margin-top: 20px; margin-left: 7px;"></i>',
            'number_post'   =>  '20',
            'radius'       =>  '20',
            'disallow_labels'   =>  'Cho phép truy cập vị trí để tìm trạm gần bạn',
            'get_directions'    =>  'Chi tiết',
            'text_open'         =>  'Mở',
            'text_phone'        =>  'Điện thoại',
            'text_hotline'      =>  'Di động',
            'text_email'        =>  'Email',
        ];

        $station_data = TblDmtram::with(['city', 'district', 'results' => function($query) use ($type) {
            $query->where('loai_so_lieu', $type);
        }])->get();

        if($type_obj->is_trendline){
            $increase_m_min = 1;
            $increase_m_max = 0;

            $decrease_m_min = 1;
            $decrease_m_max = 0;
        }else{
            $min = null;
            $max = 0;
        }


        $stations = [];
        foreach($station_data as $station){
            $results = $station->results->where('loai_so_lieu', $type);

            if($is_report_with_month){
                $results = $results->where('month', $report_month);
            }

            $results = $results->where('so_lieu', '>=', 0);

            $station->report_data = $results->pluck('so_lieu')->toArray();
            $station->report_time = $results->pluck('thoi_gian')->toArray();

            if($type_obj->is_trendline){
                $points = [];
                $i = 1;
                foreach($results as $result){
                    $points[] = [$i, $result->so_lieu];
                    $i++;
                }

                try{
                    $regression = new Regression\Linear($points);
                    $parameters = $regression->getParameters();
                    $station->m = $parameters['m'];
                    $station->equation = 'y = '.round($parameters['m'], 5).'x + '.round($parameters['b'], 5);
                } catch (\Exception $e) {
                    $station->m = 0;
                    $station->equation = '';
                    //\Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
                }

                if($station->m > 0){
                    if($station->m < $increase_m_min){
                        $increase_m_min = $station->m;
                    }
                    if($station->m > $increase_m_max){
                        $increase_m_max = $station->m;
                    }
                }elseif($station->m < 0){
                    $m_abs = abs($station->m);
                    if($m_abs < $decrease_m_min){
                        $decrease_m_min = $m_abs;
                    }
                    if($m_abs > $decrease_m_max){
                        $decrease_m_max = $m_abs;
                    }
                }
            }else{
                if($type == 'Rthang'){
                    $station->avg = round($results->sum('so_lieu'));
                }else{
                    $station->avg = round($results->avg('so_lieu'), 1);
                }

                if(!$min){
                    $min = $station->avg;
                }
                if($station->avg < $min){
                    $min = $station->avg;
                }
                if($station->avg > $max){
                    $max = $station->avg;
                }
            }

            $stations[] = $station->toArray();
        }

        if($type_obj->is_trendline){
            $increase_step = 100;
            $decrease_step = 100;

            $type_obj->increase_color_from = 'F6BDC0';
            $type_obj->increase_color_to = 'DC1C13';
            $type_obj->increase_min = $increase_m_min;
            $type_obj->increase_max = $increase_m_max;
            $increase_one_percent = ($increase_m_max - $increase_m_min) / 100;
            $increase_gradients = $this->gradient($type_obj->increase_color_from, $type_obj->increase_color_to, $increase_step);

            $type_obj->decrease_color_from = 'BFBFFF';
            $type_obj->decrease_color_to = '0000FF';
            $type_obj->decrease_min = $decrease_m_min * -1;
            $type_obj->decrease_max = $decrease_m_max * -1;
            $decrease_one_percent = ($decrease_m_max - $decrease_m_min) / 100;
            $decrease_gradients = $this->gradient($type_obj->decrease_color_from, $type_obj->decrease_color_to, $decrease_step);
        }else{
            $step = 100;

            $type_obj->min = $min;
            $type_obj->max = $max;
            $one_percent = ($max - $min) / 100;

            if(in_array($type, $amount_of_rain_types)){
                $type_obj->color_from = 'BFBFFF';
                $type_obj->color_to = '0000FF';
                $gradients = $this->gradient($type_obj->color_from, $type_obj->color_to, $step);
            }else{
                $type_obj->color_from = 'F6BDC0';
                $type_obj->color_to = 'DC1C13';
                $gradients = $this->gradient($type_obj->color_from, $type_obj->color_to, $step);
            }
        }

        if(in_array($type, $temp_types)){
            $type_obj->chart_type = 'line';
            $type_obj->backgroundColor = 'rgba(0, 0, 0, 0)';
            $type_obj->borderColor = 'rgba(249,193,15,1)';
        }else{
            $type_obj->chart_type = 'bar';
            $type_obj->backgroundColor = 'rgba(60,141,188,0.9)';
            $type_obj->borderColor = 'rgba(60,141,188,1)';
        }

        foreach($stations as $station_key => $station){
            if($type_obj->is_trendline) {
                if ($station['m'] > 0) {
                    if ($station['m'] == $increase_m_min) {
                        $icon_index = 0;
                    } else {
                        $icon_index = round(($station['m'] - $increase_m_min) / $increase_one_percent) - 1;
                    }
                    if($icon_index < 0){
                        $icon_index = 0;
                    }
                    $icon_color = $increase_gradients[$icon_index];
                    $icon_class = 'fas fa-arrow-circle-up';
                } elseif ($station['m'] < 0) {
                    $m_abs = abs($station['m']);
                    if ($m_abs == $decrease_m_min) {
                        $icon_index = 0;
                    } else {
                        $icon_index = round(($m_abs - $decrease_m_min) / $decrease_one_percent) - 1;
                    }
                    if ($icon_index < 0) {
                        $icon_index = 0;
                    }
                    $icon_color = $decrease_gradients[$icon_index];
                    $icon_class = 'fas fa-arrow-circle-down';
                } else {
                    $icon_color = 'FFFFFF';
                    $icon_class = 'fas fa-circle';
                }
                $stations[$station_key]['marker_icon'] = '
                    <div class="station-icon-box">
                        <i class="'.$icon_class.' station-icon" style="color:#'.$icon_color.';"></i>
                        <p class="station-icon-text">'.($station['m'] <> 0 ? number_format($station['m'], 3) : 0).'</p>
                    </div>';
            }else{
                if($station['avg'] > 0){
                    if($station['avg'] == $min){
                        $icon_index = 0;
                    }else{
                        $icon_index = round(($station['avg'] - $min) / $one_percent) - 1;
                    }
                    $icon_color = $gradients[$icon_index];
                }else{
                    $icon_color = 'FFFFFF';
                }

                $icon_class = 'fas fa-circle';

                $stations[$station_key]['marker_icon'] = '
                    <div class="station-icon-box">
                        <i class="'.$icon_class.' station-icon" style="color:#'.$icon_color.';"></i>
                        <p class="station-icon-text">'.(in_array($type, $amount_of_rain_types) ? number_format($station['avg']) : $station['avg']).'</p>
                    </div>';
            }
        }
        //dd($stations);

        $cities = [];
        $cities_obj = TblDmhanhchinh::with('districts')->get();
        foreach ($cities_obj as $city_key => $city_value){
            $cities[$city_key] = ['id' => $city_value->uuid, 'name' => $city_value->Ten];
            foreach ($city_value->districts as $district) {
                $cities[$city_key]['district'][] = ['id' => $district->uuid, 'name' => $district->TenQuanHuyen];
            }
        }

        $type_obj = $type_obj->toArray();

        return compact('stations', 'dvls_settings', 'cities', 'types', 'type', 'type_obj', 'report_month', 'report_months', 'is_report_with_month');
    }
}
