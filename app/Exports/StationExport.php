<?php
namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class StationExport implements FromArray, ShouldAutoSize
{
    protected $stations;

    public function __construct(array $stations)
    {
        $this->stations = $stations;
    }

    public function array(): array
    {
        return $this->stations;
    }
}
