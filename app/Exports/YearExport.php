<?php
namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Sheet;

class YearExport implements FromArray, WithEvents, WithTitle, WithStrictNullComparison
{
    private $data;

    public function  __construct(array $data)
    {
        $this->data = $data;
    }

    public function array(): array
    {

        return $this->data;
    }

    public function title(): string
    {
        return 'Báo cáo lượng mưa theo năm';
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) { 
                    $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style); 
                });

                $event->sheet->getDelegate()->mergeCells('B5:B6');
                $event->sheet->getDelegate()->mergeCells('C5:C6');
                $event->sheet->getDelegate()->mergeCells('D5:D6');
                $event->sheet->getDelegate()->mergeCells('E5:E6');
                $event->sheet->getDelegate()->mergeCells('F5:F6');
                $event->sheet->getDelegate()->mergeCells('G5:G6');
                $event->sheet->getDelegate()->mergeCells('H5:H6');
                $event->sheet->getDelegate()->mergeCells('I5:I6');
                $event->sheet->getDelegate()->mergeCells('J5:J6');
                $event->sheet->getDelegate()->mergeCells('K5:K6');
                $event->sheet->getDelegate()->mergeCells('L5:L6');
                $event->sheet->getDelegate()->mergeCells('M5:M6');

                $event->sheet->styleCells('A1:M18', [                  
                   'font' => [
                        'name' => 'Times New Roman',
                        'size' => 11,
                    ]
                ]);

                $event->sheet->styleCells('A1:M2', [
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                   'font' => [
                        'bold' => true,
                    ]
                ]);

                $event->sheet->styleCells('A5:M38', [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOUBLE,
                        ],
                    ],

                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                ]);
            },
        ];
    }
}