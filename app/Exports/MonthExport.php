<?php
namespace App\Exports;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Sheet;

class MonthExport implements FromArray, ShouldAutoSize, WithEvents, WithTitle, WithStrictNullComparison
{
    private $data;
    private $last_day;

    public function  __construct(array $data, $last_day)
    {
        $this->data = $data;
        $this->last_day = $last_day;
    }

    public function array(): array
    {

        return $this->data;
    }

    public function title(): string
    {
        return 'Báo cáo lượng mưa theo tháng';
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) { 
                    $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style); 
                });

                $event->sheet->getDelegate()->getStyle('A1:F39')->getFont()->setSize(10);
                $event->sheet->getDelegate()->mergeCells('A1:F1');
                $event->sheet->getDelegate()->mergeCells('A2:F2');
                $event->sheet->getDelegate()->mergeCells('A3:B3');
                $event->sheet->getDelegate()->mergeCells('A4:B4');
                $event->sheet->getDelegate()->mergeCells('A15:B15');
                $event->sheet->getDelegate()->mergeCells('A17:B17');
                $event->sheet->getDelegate()->mergeCells('A18:B18');
                $event->sheet->getDelegate()->mergeCells('A19:B19');
                $event->sheet->getDelegate()->mergeCells('A21:B21');
                $event->sheet->getDelegate()->mergeCells('A22:B22');
                $event->sheet->getDelegate()->mergeCells('A24:B24');

                $event->sheet->getDelegate()->mergeCells('C3:C5');
                $event->sheet->getDelegate()->mergeCells('D3:F3');
                $event->sheet->getDelegate()->mergeCells('D4:D5');
                $event->sheet->getDelegate()->mergeCells('E4:E5');
                $event->sheet->getDelegate()->mergeCells('F4:F5');

                $event->sheet->styleCells('A1:A2', [
                   'font' => [
                        'bold' => true,
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                ]);

                $event->sheet->styleCells('A3:A4', [
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                ]);

                $event->sheet->styleCells('B6', [
                    'font' => [
                        'bold' => true,
                    ],
                ]);

                $event->sheet->styleCells('B12', [
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                    ],
                ]);

                $event->sheet->styleCells('A17', [
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                ]);




                $event->sheet->styleCells('C3:F'.($this->last_day + 6), [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOUBLE,
                        ],
                    ],

                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                ]);

                $event->sheet->styleCells('C'.($this->last_day + 7).':C'.($this->last_day + 8), [
                   'font' => [
                        'bold' => true,
                    ],
                ]);

                $event->sheet->styleCells('D'.($this->last_day + 7).':D'.($this->last_day + 8), [
                   'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                    ],
                ]);
            },
        ];
    }
}