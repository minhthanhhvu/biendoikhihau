<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dulieu extends Model
{
    protected $table = 'dulieu';

    protected $appends = [
        'month',
        'year'
    ];

    public function station()
    {
        return $this->belongsTo('App\TblDmtram', 'ma_tram');
    }

    public function getThoiGianAttribute($value)
    {
        if(in_array($this->loai_so_lieu, config('app.ece.month_and_year_type'))){
            $time_array = explode('-', $value);
            $month = intval($time_array[1]);
            $year = intval($time_array[0]);
            $value = $month.'/'.$year;
        }

        return $value;
    }

    public function getMonthAttribute()
    {
        $month = null;

        if(in_array($this->loai_so_lieu, config('app.ece.month_and_year_type'))){
            $time_array = explode('/', $this->thoi_gian);
            $month = intval($time_array[0]);
        }

        return $month;
    }

    public function getYearAttribute()
    {
        $year = null;

        if(in_array($this->loai_so_lieu, config('app.ece.month_and_year_type'))){
            $time_array = explode('/', $this->thoi_gian);
            $year = intval($time_array[1]);
        }

        return $year;
    }
}
