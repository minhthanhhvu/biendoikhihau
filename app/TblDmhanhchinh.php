<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblDmhanhchinh extends Model
{
    protected $table = 'tbl_dmhanhchinh';
    protected $primaryKey = 'uuid';
    public $incrementing = false;

    public function districts()
    {
        return $this->hasMany('App\TblDmquanhuyen', 'uuidTinhTP');
    }

    public function stations()
    {
    	return $this->hasMany('App\TblDmtram', 'uuidHanhChinh');
    }
}
