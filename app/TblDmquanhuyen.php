<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblDmquanhuyen extends Model
{
    protected $table = 'tbl_dmquanhuyen';
    protected $primaryKey = 'uuid';
    public $incrementing = false;

    public function city()
    {
        return $this->belongsTo('App\TblDmhanhchinh', 'uuidTinhTP');
    }

    public function stations()
    {
    	return $this->hasMany('App\TblDmtram', 'uuidQuanHuyen');
    }
}
