<?php

namespace App\Http\Controllers;

use App\Dulieu;
use App\Loaisolieu;
use Illuminate\Http\Request;
use App\TblDmtram;
use App\TblDmhanhchinh;
use MathPHP\Statistics\Regression;
use DB;
use App\Utils\Util;

class EceController extends Controller
{
    protected $util;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Util $util)
    {
        $this->middleware('auth');
        $this->util = $util;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $result = $this->util->getReport($request, true);
        return view('ece', $result);
    }
}
